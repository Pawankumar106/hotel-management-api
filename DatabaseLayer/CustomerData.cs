﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;

namespace DatabaseLayer
{
    public class CustomerData : IDataLayerInterface
    {

        HotelManagmentEntities db = new HotelManagmentEntities();
        public void CreateCustomerData(CustomerViewModel customer)
        {
            List<CustomerViewModel> CustomerVMlist = new List<CustomerViewModel>();

            var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<CustomerViewModel, Customer>());
            IMapper mapper = config.CreateMapper();
            var customerAdd = mapper.Map<CustomerViewModel, Customer>(customer);
            db.Customers.Add(customerAdd); 
            db.SaveChanges();
        }

        public bool DublicateCustomer(CustomerViewModel customer)
        {
            var exist = db.Customers.Any(x => x.Email == customer.Email);
            return exist;

        }

        public object FindHotelData(HoteldetailsViewModel hoteldetailsViewModel)
        
        {
            List<Hotel> hotelList = (from hotel in db.Hotels
                                     join hotelroom in db.HotelsRooms
                                     on hotel.HotelId equals hotelroom.HotelId
                                     where hotel.City == hoteldetailsViewModel.City
                                     && hotel.IsActive == true
                                     && hotelroom.AvailableDate == hoteldetailsViewModel.Date
                                     && hotelroom.IsAvailable == true
                                     orderby hotel.HotelName
                                     select hotel).Distinct().ToList();

            List<HotelViewModel> hotelViewModels = new List<HotelViewModel>();
            foreach(Hotel hotel in hotelList)
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Hotel, HotelViewModel>());
                IMapper mapper = config.CreateMapper();
                var hotelVM = mapper.Map<Hotel, HotelViewModel>(hotel);
                hotelViewModels.Add(hotelVM);


            }
            return hotelViewModels;

           // int hotelId;
           // List<Hotel> hotelList = new List<Hotel>();
           // var dateTime = date;
           // var dateString = dateTime.ToString();
           //   var id = (from hotel in db.Hotels
           //           where hotel.City == city &&
           //          hotel.IsActive == true
           //           select hotel.HotelId).ToList();


           //List<int> hotelIdList = new List<int>();
           // foreach(var item in id)
           // {
           //      hotelId = (from hotelrooms in db.HotelsRooms
           //                    where hotelrooms.HotelId == item
           //                    && hotelrooms.AvailableDate == date
           //                    && hotelrooms.IsAvailable == true
           //                    select hotelrooms.HotelId).FirstOrDefault();
           //     if(hotelId != 0)
           //     {
           //         hotelIdList.Add(hotelId);
           //     }
           // }
           // foreach(var listId in hotelIdList)
           // {

           //     var list  = (from hotel in db.Hotels
           //                      where hotel.HotelId == listId
           //                      select hotel).ToList();

           //     hotelList.AddRange(list);
           // } 
           //  return hotelList;

        }

        

        public bool LoginDatabase(LoginViewModel customer)
        {
            var IsExist= db.Customers.Any(x => x.Email == customer.Email && x.Password == customer.Password);
            return IsExist;
        }

       
    }
}
