﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;

namespace DatabaseLayer
{
    public class AddRoomsDatalayer : IAddRoomsDatalayer
    {
        HotelManagmentEntities db = new HotelManagmentEntities();
        public void CreateRoomsData(RoomViewModel roomViewModel)
        {
            List<RoomViewModel> hotelViewModels = new List<RoomViewModel>();

            var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<RoomViewModel, Room>());
            IMapper mapper = config.CreateMapper();
            var roomAdd = mapper.Map<RoomViewModel, Room>(roomViewModel);
            db.Rooms.Add(roomAdd);
            db.SaveChanges();
        }

        public bool RoomsExist(RoomViewModel roomViewModel)
        {
            var isExist = db.Rooms.Any(x => x.RoomNo == roomViewModel.RoomNo);
            return isExist;
        }
    }
}
