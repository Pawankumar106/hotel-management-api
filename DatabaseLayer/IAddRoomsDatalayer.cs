﻿using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public interface IAddRoomsDatalayer
    {
        void CreateRoomsData(RoomViewModel roomViewModel);
        bool RoomsExist(RoomViewModel roomViewModel);

    }
}
