﻿using AutoMapper;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public class GetAllHotelsDataLayer : IGetAllHotelsDataLayer
    {
        HotelManagmentEntities db = new HotelManagmentEntities();

        public object FindHotelData()
        {
            List<Hotel> hotelList = (from hotel in db.Hotels
                                     orderby hotel.HotelName
                                     select hotel).Distinct().ToList();

            List<HotelViewModel> hotelViewModels = new List<HotelViewModel>();
            foreach (Hotel hotel in hotelList)
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Hotel, HotelViewModel>());
                IMapper mapper = config.CreateMapper();
                var hotelVM = mapper.Map<Hotel, HotelViewModel>(hotel);
                hotelViewModels.Add(hotelVM);


            }
            return hotelViewModels;

        }
    }
}
