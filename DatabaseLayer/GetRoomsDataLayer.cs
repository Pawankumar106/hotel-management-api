﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;

namespace DatabaseLayer
{
    public class GetRoomsDataLayer : IGetRoomsDataLayer
    {

        HotelManagmentEntities db = new HotelManagmentEntities();
        public List<RoomViewModel> FindRoomsByCateoryData(string category)
        {
            List<Room> roomList = (from room in db.Rooms
                                   where room.RoomCategory == category
                                   select room).OrderByDescending(x => x.RoomPrice).ToList();

            List<RoomViewModel> roomVMList = new List<RoomViewModel>();

            foreach (Room room in roomList)
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Room, RoomViewModel>());
                IMapper mapper = config.CreateMapper();
                var roomVm = mapper.Map<Room, RoomViewModel>(room);
                roomVMList.Add(roomVm);
            }

            return roomVMList;
        }

        public List<RoomViewModel> FindRoomsByCityData(string hotelcity)
        {
            List<Room> roomList = (from room in db.Rooms
                                   join hotel in db.Hotels on
                                    room.HotelId equals hotel.HotelId
                                   where hotel.City == hotelcity
                                   select room).OrderByDescending(x => x.RoomPrice).ToList();

            List<RoomViewModel> roomVMList = new List<RoomViewModel>();

            foreach (Room room in roomList)
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Room, RoomViewModel>());
                IMapper mapper = config.CreateMapper();
                var roomVm = mapper.Map<Room, RoomViewModel>(room);
                roomVMList.Add(roomVm);
            }

            return roomVMList;

        }

        public List<RoomViewModel> FindRoomsByPincodeData(int? pincode)
        {
            List<Room> roomList = (from room in db.Rooms
                                   join hotel in db.Hotels on
             room.HotelId equals hotel.HotelId
                                   where hotel.Pincode == pincode
                                   select room).OrderByDescending(x => x.RoomPrice).ToList();

            List<RoomViewModel> roomVMList = new List<RoomViewModel>();

            foreach (Room room in roomList)
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Room, RoomViewModel>());
                IMapper mapper = config.CreateMapper();
                var roomVm = mapper.Map<Room, RoomViewModel>(room);
                roomVMList.Add(roomVm);
            }

            return roomVMList;

        }

        public List<RoomViewModel> FindRoomsByPriceData(int? price)
        {
            List<Room> roomList = (from room in db.Rooms
                                   where room.RoomPrice == price
                                   select room).OrderByDescending(x => x.RoomPrice).ToList();

            List<RoomViewModel> roomVMList = new List<RoomViewModel>();

            foreach (Room room in roomList)
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Room, RoomViewModel>());
                IMapper mapper = config.CreateMapper();
                var roomVm = mapper.Map<Room, RoomViewModel>(room);
                roomVMList.Add(roomVm);
            }

            return roomVMList;

        }

        public List<RoomViewModel> FindRoomsData()
        {
            List<Room> roomList = (from room in db.Rooms
                                   select room).OrderByDescending(x => x.RoomPrice).ToList();

            List<RoomViewModel> roomVMList = new List<RoomViewModel>();

            foreach (Room room in roomList)
            {
                var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Room, RoomViewModel>());
                IMapper mapper = config.CreateMapper();
                var roomVm = mapper.Map<Room, RoomViewModel>(room);
                roomVMList.Add(roomVm);
            }

            return roomVMList;

        }
    }
}
