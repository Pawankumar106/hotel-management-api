﻿using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public interface IDataLayerInterface
    {
         void CreateCustomerData(CustomerViewModel customer);
        bool DublicateCustomer(CustomerViewModel customer);
        object FindHotelData(HoteldetailsViewModel hoteldetailsViewModel);
        bool LoginDatabase(LoginViewModel customer);
    }
}
