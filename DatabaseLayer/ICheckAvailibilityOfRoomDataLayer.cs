﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public interface ICheckAvailibilityOfRoomDataLayer
    {
        bool AvailableRoomData(int roomid, DateTime date);
    }
}
