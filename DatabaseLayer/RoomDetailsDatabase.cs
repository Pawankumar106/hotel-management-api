﻿using AutoMapper;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public class RoomDetailsDatabase : IRoomDetailsDataLayer
    {

        HotelManagmentEntities db = new HotelManagmentEntities();
        public object FindRoomDatabase(int hotelId)
        {
            List<Room> list = (from room in db.Rooms
                        where room.HotelId == hotelId
                        orderby room.RoomPrice
                        select room).ToList();
            List<RoomViewModel> roomViewModels = new List<RoomViewModel>();
            foreach(Room room in list)
            {

                var config = new MapperConfiguration(cfg =>
               cfg.CreateMap<Room, RoomViewModel>());
                IMapper mapper = config.CreateMapper();
                var roomList = mapper.Map<Room, RoomViewModel>(room);
                roomViewModels.Add(roomList);
            }
            return roomViewModels;
        }
    }
}
