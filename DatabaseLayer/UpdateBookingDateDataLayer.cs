﻿using AutoMapper;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public class UpdateBookingDateDataLayer : IUpdateBookingDateDataLayer
    {

        HotelManagmentEntities db = new HotelManagmentEntities();
        public bool UpdateBookingData(int id, BookingViewModel bookingVM)
        {
            var Available = (from hotelroom in db.HotelsRooms
                             where hotelroom.HotelId == bookingVM.HotelId
                             && hotelroom.RoomId == bookingVM.RoomId
                             && hotelroom.AvailableDate == bookingVM.BookingDate
                             select hotelroom.IsAvailable).SingleOrDefault();
            if (Available == true)
            {
                var config = new MapperConfiguration(cfg =>
                    cfg.CreateMap<BookingViewModel, Booking>());
                IMapper mapper = config.CreateMapper();
                var booking = mapper.Map<BookingViewModel, Booking>(bookingVM);

                db.Entry(booking).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    db.SaveChanges();
                    return true;
                }
                catch (DbUpdateConcurrencyException)
                {
                    bool var = db.Customers.Count(e => e.CustomerId == id) > 0;
                    if (!var)
                    {
                        return var;
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return false;
        }

    }
}




