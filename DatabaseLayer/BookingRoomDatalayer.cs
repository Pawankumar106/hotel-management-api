﻿using AutoMapper;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public class BookingRoomDatalayer : IBookingRoomDataLayer
    {

        HotelManagmentEntities db = new HotelManagmentEntities();
        public bool BookingRoomData(BookingViewModel bookingVM)
        {

            var hotelRoom = (from hotelroom in db.HotelsRooms
                             where hotelroom.HotelId == bookingVM.HotelId
                             && hotelroom.RoomId == bookingVM.RoomId
                             && hotelroom.AvailableDate == bookingVM.BookingDate
                             select hotelroom.IsAvailable).SingleOrDefault();
            if (hotelRoom == true)
            {
                if (bookingVM.StatusOfBooking == null)
                {
                    bookingVM.StatusOfBooking = "Optional";
                }
                bookingVM.RoomCategory = (from room in db.Rooms
                                          where room.RoomId == bookingVM.RoomId
                                          select room.RoomCategory).SingleOrDefault();

                var config = new MapperConfiguration(cfg =>
                   cfg.CreateMap<BookingViewModel, Booking>());
                IMapper mapper = config.CreateMapper();
                var booking = mapper.Map<BookingViewModel, Booking>(bookingVM);
                db.Bookings.Add(booking);
                db.SaveChanges();

                var hotelRoomId = (from hotelroom in db.HotelsRooms
                                 where hotelroom.HotelId == bookingVM.HotelId
                                 && hotelroom.RoomId == bookingVM.RoomId
                                 && hotelroom.AvailableDate == bookingVM.BookingDate
                                 select hotelroom.HotelsRoomId).SingleOrDefault();
                HotelsRoom hotels = new HotelsRoom();
                hotels.HotelsRoomId = hotelRoomId;
                hotels.HotelId = bookingVM.HotelId;
                hotels.RoomId = bookingVM.RoomId;
                hotels.AvailableDate = bookingVM.BookingDate;
                hotels.IsAvailable = !hotelRoom;
                db.Entry(hotels).State = EntityState.Modified;

                db.SaveChanges();
                return true;
            }

            return false;
        }
        public bool AllReadyBooked(BookingViewModel bookingVM)
        {
            var booked = db.Bookings.Any(x => x.HotelId == bookingVM.HotelId
                                        && x.RoomId == bookingVM.RoomId
                                        && x.BookingDate == bookingVM.BookingDate);
            return booked;
        }
    }
}
