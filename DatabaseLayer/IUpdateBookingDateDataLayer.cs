﻿using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public interface IUpdateBookingDateDataLayer
    {
        bool UpdateBookingData(int id, BookingViewModel bookingVM);
    }
}
