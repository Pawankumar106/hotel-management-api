﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public interface IRoomDetailsDataLayer
    {
        object FindRoomDatabase(int hotelId);
    }
}
