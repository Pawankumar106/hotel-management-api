﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;

namespace DatabaseLayer
{
    public class AddHotelDataLayer : IAddHotelDataLayer
    {
        HotelManagmentEntities db = new HotelManagmentEntities();
        public void CreateHotelData(HotelViewModel hotelViewModel)
        {
            List<HotelViewModel> hotelViewModels = new List<HotelViewModel>();

            var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<HotelViewModel, Hotel>());
            IMapper mapper = config.CreateMapper();
            var hotelAdd = mapper.Map<HotelViewModel, Hotel>(hotelViewModel);
            db.Hotels.Add(hotelAdd);
            db.SaveChanges();

        }

        public bool HotelExist(HotelViewModel hotelViewmodel)
        {
            var isExist = db.Hotels.Any(x => x.HotelName == hotelViewmodel.HotelName
                          && x.Address == hotelViewmodel.Address);
            return isExist;
        }
    }
}
