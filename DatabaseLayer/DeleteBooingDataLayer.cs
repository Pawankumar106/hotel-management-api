﻿using AutoMapper;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public class DeleteBooingDataLayer : IDeleteBookingDataLayer
    {
        public BookingViewModel CancleBookingData(int id)
        {

            HotelManagmentEntities db = new HotelManagmentEntities();
            Booking booking = db.Bookings.Find(id);

            var config = new MapperConfiguration(cfg =>
                cfg.CreateMap<Booking, BookingViewModel>());
            IMapper mapper = config.CreateMapper();
            var bookingVm = mapper.Map<Booking, BookingViewModel>(booking);

            if (booking != null)
            {
                db.Bookings.Remove(booking);
                db.SaveChanges();
            }
            var hotelRoomId = (from hotelroom in db.HotelsRooms
                               where hotelroom.HotelId == booking.HotelId
                               && hotelroom.RoomId == booking.RoomId
                               && hotelroom.AvailableDate == booking.BookingDate
                               select new { hotelroom.HotelsRoomId ,
                               hotelroom.IsAvailable}).ToList();
            HotelsRoom hotels = new HotelsRoom();
            hotels.HotelsRoomId = hotelRoomId[0].HotelsRoomId;
            hotels.HotelId = booking.HotelId;
            hotels.RoomId = booking.RoomId;
            hotels.AvailableDate = booking.BookingDate;
            hotels.IsAvailable = !hotelRoomId[0].IsAvailable;
            db.Entry(hotels).State = EntityState.Modified;

            db.SaveChanges();

            return bookingVm;
        }

    }
}
