﻿using EntityLayer.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseLayer
{
    public class CheckAvailibilityOfRoomDataLayer : ICheckAvailibilityOfRoomDataLayer
    {

        HotelManagmentEntities db = new HotelManagmentEntities();
        public bool AvailableRoomData(int roomid, DateTime date)
        {
            bool exist = db.Bookings.Any(x => x.RoomId == roomid && x.BookingDate == date);

            return !exist;
        }
    }
}
