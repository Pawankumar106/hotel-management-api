﻿using DatabaseLayer;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class UpdateBookingDateBusiness : IUpdateBookingDateBusiness
    {
        IUpdateBookingDateDataLayer updateBookingDateDataLayer;
        public UpdateBookingDateBusiness(IUpdateBookingDateDataLayer _updateBookingDateDataLayer)
        {
            updateBookingDateDataLayer = _updateBookingDateDataLayer;
        }
        public bool UpdateBookingBusiness(int id, BookingViewModel bookingVM)
        {
           
            return updateBookingDateDataLayer.UpdateBookingData(id, bookingVM);
        }

    }
}
