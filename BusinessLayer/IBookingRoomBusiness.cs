﻿using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IBookingRoomBusiness
    {
        bool BookingRoom(BookingViewModel bookingVM);
        bool AllReadyBooked(BookingViewModel bookingVM);
    }
}
