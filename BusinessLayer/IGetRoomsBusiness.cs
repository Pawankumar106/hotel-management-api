﻿using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IGetRoomsBusiness
    {
        List<RoomViewModel> FindRoomsData();
        List<RoomViewModel> FindRoomsByCityData(string hotelcity);
        List<RoomViewModel> FindRoomsByPriceData(int? price);
        List<RoomViewModel> FindRoomsByCateoryData(string category);
        List<RoomViewModel> FindRoomsByPincodeData(int? pincode);

    }
}
