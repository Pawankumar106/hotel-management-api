﻿using DatabaseLayer;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class AddHotelBusiness : IAddHotelBusiness
    {
        IAddHotelDataLayer Iaddhoteldatalayer;
        public AddHotelBusiness(IAddHotelDataLayer addHotelDataLayer)
        {
            Iaddhoteldatalayer = addHotelDataLayer;
        }

        public bool CreateHotel(HotelViewModel hotelViewmodel)
        {
            var IsExist = Iaddhoteldatalayer.HotelExist(hotelViewmodel);
            if(IsExist == false)
            {
                Iaddhoteldatalayer.CreateHotelData(hotelViewmodel);
            }
            return IsExist;
        }
    }
}
