﻿using DatabaseLayer;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BookingRoomBusiness : IBookingRoomBusiness
    {

        IBookingRoomDataLayer bookingRoomDataLayer;
        public BookingRoomBusiness(IBookingRoomDataLayer _bookingRoomDataLayer)
        {
            bookingRoomDataLayer = _bookingRoomDataLayer;
        }
        public bool BookingRoom(BookingViewModel bookingVM)
        {
            var Available = bookingRoomDataLayer.BookingRoomData(bookingVM);
            return Available;
        }
         
        public bool AllReadyBooked(BookingViewModel bookingVM)
        {
            var booked = bookingRoomDataLayer.AllReadyBooked(bookingVM);
            return booked;
        }
    }
}
