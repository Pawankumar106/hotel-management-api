﻿using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IBusinessLayerCustomer
    {
        bool LogincustomerBusiness(LoginViewModel customer);
        object FindHotelBusiness(HoteldetailsViewModel hoteldetailsViewModel);
        bool CreateCustomerBusiness(CustomerViewModel customer);


    }
}
