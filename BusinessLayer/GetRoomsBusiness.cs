﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseLayer;
using EntityLayer.Model.ViewModel;

namespace BusinessLayer
{
    public class GetRoomsBusiness : IGetRoomsBusiness
    {
        IGetRoomsDataLayer getRoomsDataLayer;
        public GetRoomsBusiness(IGetRoomsDataLayer _getRoomsDataLayer)
        {
            getRoomsDataLayer = _getRoomsDataLayer;
        }

        public List<RoomViewModel> FindRoomsByCateoryData(string category)
        {
            List<RoomViewModel> roomList = getRoomsDataLayer.FindRoomsByCateoryData(category);
            return roomList;

        }

        public List<RoomViewModel> FindRoomsByCityData(string hotelcity)
        {
            List<RoomViewModel> roomList = getRoomsDataLayer.FindRoomsByCityData(hotelcity);
            return roomList;

        }

        public List<RoomViewModel> FindRoomsByPincodeData(int? pincode)
        {
            List<RoomViewModel> roomList = getRoomsDataLayer.FindRoomsByPincodeData(pincode);
            return roomList;

        }

        public List<RoomViewModel> FindRoomsByPriceData(int? price)
        {
            List<RoomViewModel> roomList = getRoomsDataLayer.FindRoomsByPriceData(price);
            return roomList;

        }

        public List<RoomViewModel> FindRoomsData()
        {
            List<RoomViewModel> roomList = getRoomsDataLayer.FindRoomsData();
            return roomList;

        }
    }
}
