﻿using DatabaseLayer;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class CustomerBusiness : IBusinessLayerCustomer

    {
        IDataLayerInterface  ICustomerData;
        public CustomerBusiness(IDataLayerInterface dataLayerInterface)
        {
            ICustomerData = dataLayerInterface;
        }
        //var  ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        HotelManagmentEntities db = new HotelManagmentEntities();
        public bool CreateCustomerBusiness(CustomerViewModel customer)
        {

           // CustomerData customerData = new CustomerData();
            var isEmailAlreadyExists = ICustomerData.DublicateCustomer(customer);
             
            if (isEmailAlreadyExists == false)
            {
                ICustomerData.CreateCustomerData(customer);
            }
           return isEmailAlreadyExists;

            
        }

        public object FindHotelBusiness(HoteldetailsViewModel hoteldetailsViewModel)
        {
           // CustomerData customerData = new CustomerData();
            var hotelList = ICustomerData.FindHotelData(hoteldetailsViewModel);
            return hotelList;

        }

        

        public bool LogincustomerBusiness(LoginViewModel customer)
        {
           // CustomerData customerData = new CustomerData();
            var Isexist = ICustomerData.LoginDatabase(customer);
            return Isexist;


        }
    }
}
