﻿using DatabaseLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class GetAllHotelsBusiness : IGetAllHotelsBusiness
    {
        IGetAllHotelsDataLayer getAllHotelsDataLayer;
        public GetAllHotelsBusiness(IGetAllHotelsDataLayer _getAllHotelsDataLayer)
        {
            getAllHotelsDataLayer = _getAllHotelsDataLayer;
        }

        public object FindHotelData()
        {
            var hotels = getAllHotelsDataLayer.FindHotelData();
            return hotels;
        }
    }
}
