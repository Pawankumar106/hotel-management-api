﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseLayer;
using EntityLayer.Model.ViewModel;

namespace BusinessLayer
{
    public class AddRoomsBusiness : IAddRoomsBusiness
    {
        IAddRoomsDatalayer addRoomsDatalayer;
        public AddRoomsBusiness(IAddRoomsDatalayer _addRoomsDatalayer)
        {
            addRoomsDatalayer = _addRoomsDatalayer;
        }

        public bool CreateRooms(RoomViewModel roomViewModel)
        {
            var IsExist = addRoomsDatalayer.RoomsExist(roomViewModel);
            if (IsExist == false)
            {
                addRoomsDatalayer.CreateRoomsData(roomViewModel);
            }
            return IsExist;

        }
    }
}
