﻿using DatabaseLayer;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class DeleteBookingBusiness : IDeleteBookingBusiness
    {
        IDeleteBookingDataLayer deleteBookingDataLayer;
        public DeleteBookingBusiness(IDeleteBookingDataLayer _deleteBookingDataLayer)
        {
            deleteBookingDataLayer = _deleteBookingDataLayer;
        }
        public BookingViewModel CancleBookingBusiness(int id)
        {
            BookingViewModel booking = deleteBookingDataLayer.CancleBookingData(id);
            return booking;
        }

    }
}
