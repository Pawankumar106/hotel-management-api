﻿using DatabaseLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class CheckAvailibilityOfRoomBusiness : ICheckAvailibilityOfRoomBusiness
    {

        ICheckAvailibilityOfRoomDataLayer checkAvailibilityOfRoomData;
        public CheckAvailibilityOfRoomBusiness(ICheckAvailibilityOfRoomDataLayer _checkAvailibilityOfRoomData)
        {
            checkAvailibilityOfRoomData = _checkAvailibilityOfRoomData;
        }
        public bool AvailableRoomBusiness(int roomid, DateTime date)
        {
          
            bool isAvailable = checkAvailibilityOfRoomData.AvailableRoomData(roomid, date);

            return isAvailable;
        }
    }
}
