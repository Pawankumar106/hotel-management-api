﻿using DatabaseLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class RoomDetailsBusiness : IRoomDetailsBusiness
    {
        IRoomDetailsDataLayer roomDetailsDataLayer;
        public RoomDetailsBusiness(IRoomDetailsDataLayer _roomDetailsDataLayer)
        {
            roomDetailsDataLayer = _roomDetailsDataLayer;
        }
        public object FindRoomBusiness(int hotelId)
        {
            var roomList = roomDetailsDataLayer.FindRoomDatabase(hotelId);
            return roomList;
        }
    }
}
