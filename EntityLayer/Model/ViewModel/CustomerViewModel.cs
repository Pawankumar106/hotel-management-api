﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Model.ViewModel
{
   public  class CustomerViewModel
    {
        public int CustomerId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public long ContactNo { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Password { get; set; }
    }
}
