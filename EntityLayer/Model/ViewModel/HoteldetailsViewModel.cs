﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Model.ViewModel
{
    public class HoteldetailsViewModel
    {
        public string City { get; set; }

        public DateTime Date { get; set; }
    }
}
