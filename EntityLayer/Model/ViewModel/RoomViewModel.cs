﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Model.ViewModel
{
    public class RoomViewModel
    {
        public int RoomId { get; set; }
        public int HotelId { get; set; }
        public int RoomNo { get; set; }
        public string RoomCategory { get; set; }
        public decimal RoomPrice { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Createdby { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string Updatedby { get; set; }


    }
}
