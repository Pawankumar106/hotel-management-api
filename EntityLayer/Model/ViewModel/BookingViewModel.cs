﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLayer.Model.ViewModel
{
    public class BookingViewModel
    {

        public int BookingId { get; set; }
        public int HotelId { get; set; }
        public int RoomId { get; set; }
        public System.DateTime BookingDate { get; set; }
        public string StatusOfBooking { get; set; }
        public string RoomCategory { get; set; }
    }
}
