﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HotelManagment.Controllers
{
    public class CustomerMVCController : Controller
    {
        // GET: Customer
        public ActionResult Customer()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult HotelDetails()
        {
            return View();
        }
        public ActionResult Rooms()
        {
            return View();
        }
    }
}