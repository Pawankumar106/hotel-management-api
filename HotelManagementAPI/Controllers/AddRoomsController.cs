﻿using BusinessLayer;
using EntityLayer.Model.ViewModel;
using HotelManagementAPI.BasicAuthenticationFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HotelManagementAPI.Controllers
{
    [BasicAuthentication]
    public class AddRoomsController : ApiController
    {
        private IAddRoomsBusiness addRoomsBusiness;
        public AddRoomsController(IAddRoomsBusiness _addRoomsBusiness)
        {
            addRoomsBusiness = _addRoomsBusiness;
        }
        [Route("api/AddRooms")]
        public IHttpActionResult PostHotel(RoomViewModel room)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Success = "Room Added!";
            //  CustomerBusiness customerBusiness = container.Resolve<CustomerBusiness>();
            var Isexist = addRoomsBusiness.CreateRooms(room);
            if (Isexist)
            {
                return Content(HttpStatusCode.BadRequest, "room is all ready exist in database!");
            }
            //return CreatedAtRoute("DefaultApi", new { id = customer.CustomerId }, customer);
            return Ok(Success);
        }

    }
}

