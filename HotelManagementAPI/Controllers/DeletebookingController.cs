﻿using BusinessLayer;
using EntityLayer.Model.ViewModel;
using HotelManagementAPI.BasicAuthenticationFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HotelManagementAPI.Controllers
{

    [BasicAuthentication]
    public class DeletebookingController : ApiController
    {

        private IDeleteBookingBusiness deleteBookingBusiness;
        public DeletebookingController(IDeleteBookingBusiness _deleteBookingBusiness)
        {
            deleteBookingBusiness = _deleteBookingBusiness;
        }
        [HttpDelete]
        [Route("api/Deletebooking/")]
        public IHttpActionResult DeletBooking(int id)
        {
        BookingViewModel booking = deleteBookingBusiness.CancleBookingBusiness(id);
            if (booking == null)
            {
                return NotFound();
            }

            return Ok(booking);
        }
    }
}
