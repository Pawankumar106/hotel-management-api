﻿using BusinessLayer;
using EntityLayer.Model.Entity;
using EntityLayer.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Unity;

namespace HotelManagementAPI.Controllers
{
    public class CustomerController : ApiController
    {
        private IBusinessLayerCustomer Ibusinesslayercustomer;
        public CustomerController(IBusinessLayerCustomer businessLayerCustomer)
        {
            Ibusinesslayercustomer = businessLayerCustomer;
        }

        [Route("api/Customer")]
        public IHttpActionResult PostCustomer(CustomerViewModel customer)
        {   
           
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
          //  CustomerBusiness customerBusiness = container.Resolve<CustomerBusiness>();
            var Isexist = Ibusinesslayercustomer.CreateCustomerBusiness(customer);
            if (Isexist)
            {
                return Content(HttpStatusCode.BadRequest, "User with this email already exists. Please provide another Email.");
             }
            return CreatedAtRoute("DefaultApi", new { id = customer.CustomerId }, customer);

        }

    }
}
