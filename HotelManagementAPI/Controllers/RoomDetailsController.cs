﻿using BusinessLayer;
using HotelManagementAPI.BasicAuthenticationFilter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HotelManagementAPI.Controllers
{
    [BasicAuthentication]
    public class RoomDetailsController : ApiController
    {
        private IRoomDetailsBusiness roomDetailsBusiness;
        public RoomDetailsController(IRoomDetailsBusiness _roomDetailsBusiness)
        {
            roomDetailsBusiness = _roomDetailsBusiness;
        }
        [Route("api/RoomDetails/")]
        public IHttpActionResult GetRooms(int HotelId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Object hotelList = roomDetailsBusiness.FindRoomBusiness(HotelId);
            var itemObject = JsonConvert.SerializeObject(hotelList,
                   Formatting.None,
                   new JsonSerializerSettings()
                   {
                       ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                   });
            // var array = hotelList as string[];
            if (hotelList == null)
            {
                return Content(HttpStatusCode.BadRequest, "Room is Not available for your choose on date in hotel!");
            }

            return Ok(itemObject);
        }

    }
}
