﻿using BusinessLayer;
using EntityLayer.Model.ViewModel;
using HotelManagementAPI.BasicAuthenticationFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HotelManagementAPI.Controllers
{
    [BasicAuthentication]
    public class AddHotelsController : ApiController
    {
        private IAddHotelBusiness addHotelBusiness;
        public AddHotelsController(IAddHotelBusiness _addHotelBusiness)
        {
            addHotelBusiness = _addHotelBusiness;
        }
        [Route("api/AddHotels")]
        public IHttpActionResult PostHotel(HotelViewModel hotel)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var Success = "Hotel Added!";
            //  CustomerBusiness customerBusiness = container.Resolve<CustomerBusiness>();
            var Isexist = addHotelBusiness.CreateHotel(hotel);
            if (Isexist)
            {
                return Content(HttpStatusCode.BadRequest, "Hotel is all ready exist in database!");
            }
            //return CreatedAtRoute("DefaultApi", new { id = customer.CustomerId }, customer);
            return Ok(Success);
        }

    }
}
