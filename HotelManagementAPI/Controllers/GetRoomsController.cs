﻿using BusinessLayer;
using EntityLayer.Model.ViewModel;
using HotelManagementAPI.BasicAuthenticationFilter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace HotelManagementAPI.Controllers
{
    [BasicAuthentication]
    public class GetRoomsController : ApiController
    {
        private IGetRoomsBusiness getRoomsBusiness;
        public GetRoomsController(IGetRoomsBusiness _getRoomsBusiness)
        {
            getRoomsBusiness = _getRoomsBusiness;
        }

        [Route("api/GetRooms")]
        [HttpGet]
        public IHttpActionResult GetRooms(string hotelcity = null, int? pincode = 0, int? price = 0, string category = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
           // CustomerBusiness customerBusiness = new CustomerBusiness();
            List<RoomViewModel> roomList = new List<RoomViewModel>();
            //Object hotelList=customerBusiness.FindHotelBusiness(city, date);
            if (hotelcity == null && price == 0 && pincode == 0 && category == null)
            {
                roomList = getRoomsBusiness.FindRoomsData();
            }
            else if (hotelcity != null)
            {
                roomList = getRoomsBusiness.FindRoomsByCityData(hotelcity);
            }
            else if (pincode != 0)
            {
                roomList = getRoomsBusiness.FindRoomsByPincodeData(pincode);
            }
            else if (price != 0)
            {
                roomList = getRoomsBusiness.FindRoomsByPriceData(price);
            }
            else if (category != null)
            {
                roomList = getRoomsBusiness.FindRoomsByCateoryData(category);
            }


            if (roomList == null)
            {
                return Content(HttpStatusCode.BadRequest, "Room is not available for your required date!");
            }
            else
            {
                var itemObject = JsonConvert.SerializeObject(roomList,
                       Formatting.None,
                       new JsonSerializerSettings()
                       {
                           ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                       });


                return Ok(itemObject);
            }
        }

    }
}
